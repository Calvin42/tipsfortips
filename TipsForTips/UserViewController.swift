//
//  SecondViewController.swift
//  TipsForTips
//
//  Created by Claudio Kerov on 05/05/16.
//  Copyright © 2016 Claudio Kerov. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
/*
 Controller per gestire l'utente
 Se deve accedere o fare la registrazione non puo' fare nulla
 */
import UIKit

class UserViewController: UIViewController {
    
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    var user: User?
    var server: String?
    var menuvc: MenuViewController?
    var base: String?
    var anon: String?
    
    /*****************************
     *    STRING LOCALIZATION    *
     *****************************/
    
    func updateViewElementsLanguage() {
        self.navigationItem.title = (self.menuvc?.userPage)!

        self.loginButton.setTitle((self.menuvc?.signin)!, forState: UIControlState.Normal)
        self.loginButton.backgroundColor = UIColor(red: 102/255, green: 213/255, blue: 99/255, alpha: 1)

        self.logoutButton.setTitle((self.menuvc?.logout)!, forState: UIControlState.Normal)
        self.logoutButton.backgroundColor = UIColor(red: 226/255, green: 69/255, blue: 76/255, alpha: 1)
        
        self.listButton.setTitle((self.menuvc?.listLabel)!, forState: UIControlState.Normal)

    }
    
    func updateLabel() {
        if self.menuvc?.currentUser != nil && self.menuvc?.currentUser?.userName.isEmpty == false {
            username.text = "\((self.menuvc?.hello)!) \((self.menuvc?.currentUser!.userName)!)"
        } else {
            username.text = "\((self.menuvc?.hello)!) \((self.menuvc?.anon)!)"
        }
    }
    
    /*****************************/
    /*      Useful Methods       */
    /*****************************/
    

    @IBAction func showList(sender: UIButton) {
    }
    
    /*****************************/
    /*    Controller Methods     */
    /*****************************/
    
    func switchButtons(isLoggedIn: Bool) {
        listButton.hidden = !isLoggedIn
        loginButton.hidden = isLoggedIn
        logoutButton.hidden = !isLoggedIn
    }
   
    @IBAction func goToLogin(sender: UIButton) {
        self.performSegueWithIdentifier("loggedIn", sender: self)
    }
    
    @IBAction func logoutButtonAction(sender: UIButton) {
        let alert = self.menuvc?.createAlert("", msg: (self.menuvc?.sureToLogout)!, action: (self.menuvc?.no)!, callback:  nil)
        alert?.addAction(UIAlertAction(title: (self.menuvc?.yesIam)!, style: .Default, handler: {
            (alert: UIAlertAction!) in
                self.logout()
        }))
        presentViewController(alert!, animated: true, completion: nil)
    }
    
    func logout() {
        self.menuvc?.isUserLoggedIn = false
        switchButtons((self.menuvc?.isUserLoggedIn)!)
        self.menuvc?.currentUser = nil
        updateLabel()
    }
    @IBAction func unwindToUSer(segue: UIStoryboardSegue) {
        menuvc?.isUserLoggedIn = true
        updateLabel()
        self.switchButtons(true)
    }

    override func viewWillAppear(animated: Bool) {
        updateLabel()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let mvc = self.tabBarController as! MenuViewController
        menuvc = mvc
        self.menuvc?.setBackGroundColorForView(self.view)
//        self.tabBarItem.image = UIImage(named: "contact")
        self.menuvc?.tabBar.backgroundColor = UIColor(red: 179/255, green: 179/255, blue: 179/255, alpha: 1.0)
        updateViewElementsLanguage()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "loggedIn" {
//            print("Passo per qui")
            let loginvc = segue.destinationViewController as! LoginViewController
            loginvc.uservc = self
            
        }
        if segue.identifier == "tipsList" {
            let listvc = segue.destinationViewController as! TipListTableViewController
//            listvc.currentUser = self.user!.copy() as? User
            listvc.server = self.server
            listvc.menuvc = self.menuvc
        }
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.server = menuvc?.server
    }

}

