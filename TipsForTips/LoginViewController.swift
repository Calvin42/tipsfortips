//
//  ViewController.swift
//  TipsForTips
//
//  Created by riccardo on 24/06/16.
//  Copyright © 2016 Claudio Kerov. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import UIKit
import Alamofire




class LoginViewController: UIViewController, UITextFieldDelegate {
    var menuvc: MenuViewController?
    var uservc: UserViewController?
    
    @IBOutlet weak var loginEmail: UITextField!
    @IBOutlet weak var loginPassword: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var goToRegistration: UIButton!
    
    
    var user = User()
    var server: String?
    var isUserLoggedIn = false
    
    
    /*****************************
     *    STRING LOCALIZATION    *
     *****************************/
    
    func updateViewElementsLanguage() {
        self.loginLabel.text = self.menuvc?.signin
        self.loginButton.setTitle(self.menuvc?.signin, forState: .Normal)
        self.goToRegistration.setTitle(self.menuvc?.toRegistration, forState: .Normal)
    }
    
    /************************
     *    USEFUL METHODS    *
     ************************/
    func createAlert(title: String, msg: String, action: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: action, style: UIAlertActionStyle.Default, handler: nil))
        
        return alert
    }
    
    // it may have multiples parameters and check for more characters, but not now
    func clean(what: String) -> String {
        var tmp = what.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        tmp = tmp.stringByReplacingOccurrencesOfString("(", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        tmp = tmp.stringByReplacingOccurrencesOfString(")", withString: "", options:  NSStringCompareOptions.LiteralSearch, range: nil)
        return tmp.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    
    
    /**********************/
    /*    AJAX METHODS    */
    /**********************/

    
    func execLogin(parameters: [String: String]) {
        Alamofire.request(.GET, self.server!+"/login", parameters: parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success(let JSON):
                    let res = JSON as! NSDictionary
                    let val = res.objectForKey("result")!
                    if String(val) != "ERROR" {
                        let userEmail = parameters["email"]!
                        let userName = self.clean(String(val))
                        self.menuvc?.currentUser = User(name: userName, email: userEmail)
                        self.menuvc?.isUserLoggedIn = true
                        self.performSegueWithIdentifier("unwindToUser", sender: self)

                    }else {
                        let alert = self.createAlert((self.menuvc?.error)!, msg: (self.menuvc?.wrongCredentials)!, action: (self.menuvc?.tryAgain)!)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }

                    return
                case .Failure(let error):
                    print (error)
                    self.menuvc?.presentAlertForError(error.code, this: self)
                    return
                }
        }
        

    }
    
    
    func login(email: String, pw: String) {
        let loginParameters = [
            "email": email,
            "password": pw
        ]
        execLogin(loginParameters)
    }
    
    /************
     * BUTTONS  *
     ************/
    
    @IBAction func showRegistrationForm(sender: UIButton) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewControllerWithIdentifier("Registration") as! RegisterViewController
            self.performSegueWithIdentifier("register", sender: self)
    }
    

    
    
    @IBAction func goBack(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func checkAndSendLabels() -> (String, String) {
        var emailToSend = ""
        var pwToSend = ""
        if let email = loginEmail.text {
            if email.isEmpty {
                let alert = createAlert((self.menuvc?.missingEmail)!, msg: "\((self.menuvc?.haveToInsert)!) \((self.menuvc?.anEmail)!)", action: (self.menuvc?.errorGotIt)!)
                presentViewController(alert, animated: true, completion: nil)
                return ("", "")
            }
            emailToSend = email
        }
        if let pw = loginPassword.text {
            if pw.isEmpty {
                let alert = createAlert((self.menuvc?.missingPassword)!, msg: (self.menuvc?.essentialPassword)!, action: (self.menuvc?.errorGotIt)!)
                presentViewController(alert, animated: true, completion: nil)
                return ("","")
            }
            pwToSend = pw
        }
    
        return (emailToSend, pwToSend)
    } // end of checkAndSendLabels
    
    @IBAction func loginAction(sender: UIButton) {
        let (checkedEmail, checkedPw) = checkAndSendLabels()
        if checkedEmail != "" && checkedPw != "" {
            login(checkedEmail, pw: checkedPw)
        }
    }
    
    
    /***********************/
    /*  CONTROLLER METHODS */
    /***********************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginEmail.delegate = self
        loginPassword.delegate = self
        self.server = self.uservc?.server
        self.menuvc = self.uservc?.menuvc
        self.menuvc?.setBackGroundColorForView(self.view)
        updateViewElementsLanguage()
    }
    
    
    // quando viene toccato un punto qualsiasi della view
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "unwindToUser" {
            let destvc = segue.destinationViewController as! UserViewController
            destvc.user = self.user.copy() as? User
            return
        }
        if segue.identifier == "register" {
            let destvc = segue.destinationViewController as! RegisterViewController
            destvc.loginvc = self
            destvc.menuvc = self.menuvc
            destvc.uservc = self.uservc
            destvc.server = self.server
        }
        
    }
    /***********************/
    /*  TEXTFIELD METHODS */
    /***********************/
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == loginPassword {
            textField.resignFirstResponder()
            loginButton.enabled = false
            let (checkedEmail, checkedPw) = checkAndSendLabels()
            if checkedEmail != "" && checkedPw != "" {
                login(checkedEmail, pw: checkedPw)
                loginButton.enabled = true
            }
            return false
        }
        if textField == loginEmail {
            
            let nextTag: Int = textField.tag + 1;
            // Try to find next responder
            if let nextResponder: UIResponder! = textField.superview!.viewWithTag(nextTag){
                nextResponder.becomeFirstResponder()
            }
            else {
                // Not found, so remove keyboard.
                textField.resignFirstResponder()
            }
        }
        return false
    } // end of textFieldShouldReturn
    // magari utile per controllare l'input, magari no
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == loginEmail {
            //            print("Login email")
        }
        return true
    }

}
