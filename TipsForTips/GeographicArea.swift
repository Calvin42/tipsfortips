//
//  GeographicArea.swift
//  TipsForTips
//
//  Created by claudio on 14/05/16.
//  Copyright © 2016 Claudio Kerov. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

class GeographicArea: NSObject, NSCopying {
    var city = String()
    var administrativeArea = String()
    var country = String()
    var locationDescription = String()
    var latitude = String()
    var longitude = String()
    
    init(city: String, administrativeArea: String, country: String, description: String, latitude: String, longitude: String) {
        self.city = city
        self.administrativeArea = administrativeArea
        self.country = country
        self.locationDescription = description
        self.latitude = latitude
        self.longitude = longitude
        
    }
    
    override init() {
        self.city = ""
        self.administrativeArea = ""
        self.country = ""
        self.locationDescription = ""
        
    }
    @objc func copyWithZone(zone: NSZone) -> AnyObject {
        let copy = GeographicArea(city: city, administrativeArea: administrativeArea, country: country, description: locationDescription, latitude: latitude, longitude: longitude)
        return copy
    }
    
    /****************/
    /*   METHODS    */
    /****************/
    
    func getParameters() -> [String: String] {
        let langId = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode) as! String
        return {["longitude": self.longitude, "latitude": self.latitude, "language": langId]}()
    }

}