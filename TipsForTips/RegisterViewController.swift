//
//  RegisterViewController.swift
//  TipsForTips
//
//  Created by Claudio Kerov on 05/08/16.
//  Copyright © 2016 Claudio Kerov. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var registerEmail: UITextField!
    @IBOutlet weak var registerUsername: UITextField!
    @IBOutlet weak var registerPassword: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var backToLogin: UIButton!

    
    var server: String?
    
//    var user: User?
    var loginvc: LoginViewController?
    var uservc: UserViewController?
    var menuvc: MenuViewController?
    
    /*****************************
     *    STRING LOCALIZATION    *
     *****************************/
    
    func updateViewElementsLanguage() {
        self.registerLabel.text = (self.menuvc?.signup)!
        self.registerButton.setTitle((self.menuvc?.signup)!, forState: .Normal)
        self.backToLogin.setTitle((self.menuvc?.backToLogin)!, forState: .Normal)
    }
    
    
    @IBAction func showLoginForm(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        var result: Bool?
        let emailRegEx = "[A-Z0-9a-z._]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        result = emailTest.evaluateWithObject(testStr)
        return result!
    }
    
    
    func register(parameters: [String: String]) {
        Alamofire.request(.POST, self.server!+"/register", parameters: parameters)
        .validate()
        .responseJSON { response in
            switch response.result {
            case .Success(let JSON):
                let res = JSON as! NSDictionary
                let val = res.objectForKey("response")
                if String(val) != "ERROR" {
                    let alert = self.menuvc?.createAlert("", msg: (self.menuvc?.registrationComplete)!, action: (self.menuvc?.yay)!, callback: {(alert: UIAlertAction!) in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                    self.presentViewController(alert!, animated: true, completion: nil)
                } else {
                    let alert = self.menuvc?.createAlert("", msg: (self.menuvc?.alreadyUsed)!, action: (self.menuvc?.errorOkay)!, callback: nil)
                    self.presentViewController(alert!, animated: true, completion: nil)
                }
                return
            case .Failure(let error):
                print (error)
                self.menuvc?.presentAlertForError(error.code, this: self)
                
            }
        }
    }
    
    /*
        REGISTRAZIONE VIA BOTTONE
     */
    @IBAction func doRegistration(sender: UIButton) {
        if ((registerEmail.text?.isEmpty)!) {
            let alert = self.menuvc?.createAlert((self.menuvc?.errorOops)!, msg: "\((self.menuvc?.needAn)!)\((self.menuvc?.email)!)!", action: (self.menuvc?.errorGotIt)!, callback: nil)
            self.presentViewController(alert!, animated: true, completion: nil)
            return
        } else if !isValidEmail((registerEmail.text)!) {
            let alert = self.menuvc?.createAlert((self.menuvc?.miswritten)!, msg: (self.menuvc?.invalidEmail)!, action: (self.menuvc?.errorGotIt)!, callback: nil)
            self.presentViewController(alert!, animated: true, completion: nil)
        }
        if ((registerPassword.text?.isEmpty)!) {
            let alert = self.menuvc?.createAlert((self.menuvc?.errorOops)!, msg: "\((self.menuvc?.needA)!)\((self.menuvc?.password)!)!", action: (self.menuvc?.errorGotIt)!, callback: nil)
            self.presentViewController(alert!, animated: true, completion: nil)
            return
        }
        if ((registerUsername.text?.isEmpty)!) {
            let alert = self.menuvc?.createAlert((self.menuvc?.errorOops)!, msg: "\((self.menuvc?.needA)!)\((self.menuvc?.username)!)!", action: (self.menuvc?.errorGotIt)!, callback: nil)
            self.presentViewController(alert!, animated: true, completion: nil)
            return
        }
        registerButton.enabled = false
        let registerParameters = [
            "username": registerUsername.text!,
            "password": registerPassword.text!,
            "email": registerEmail.text!
        ]

        register(registerParameters)
    }
    
    // quando viene premuto return sulla tastiera
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == registerPassword {
            textField.resignFirstResponder()
            
            registerButton.enabled = false
            let registerParameters = [
                "username": registerUsername.text!,
                "password": registerPassword.text!,
                "email": registerEmail.text!
            ]

            register(registerParameters)
        }
        
        if textField == registerEmail || textField == registerUsername {
            let nextTag: Int = textField.tag + 1;
            // Try to find next responder
            if let nextResponder: UIResponder! = textField.superview!.viewWithTag(nextTag){
                nextResponder.becomeFirstResponder()
            }
            else {
                // Not found, so remove keyboard.
                textField.resignFirstResponder()
            }
        }
        
        return false
    }
    // magari utile per controllare l'input, magari no
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == registerEmail {
//            print("Register email")
        } else if textField == registerPassword {
//            print("Register password")
        }
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerEmail.delegate = self
        registerUsername.delegate = self
        registerPassword.delegate = self
        self.menuvc?.setBackGroundColorForView(self.view)
        updateViewElementsLanguage()
        
    }
    
    // quando viene toccato un punto qualsiasi della view
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
