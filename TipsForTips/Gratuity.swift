//
//  Gratuity.swift
//  TipsForTips
//
//  Created by Claudio Kerov on 24/06/16.
//  Copyright © 2016 Claudio Kerov. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import Foundation
import CoreLocation

class Gratuity: NSObject, NSCopying {
    var value = Int()
    var locationDescription = String()
    var email = String()
    var data = String()
    
    init(value: Int, locationDescription: String, email: String, data: String) {
        self.value = value
        self.email = email
        self.locationDescription = locationDescription
        self.data = data
    }
    
    override init() {
        self.value = 0
        self.locationDescription = ""
        self.email = ""
        self.data = ""
    }
    
    @objc func copyWithZone(zone: NSZone) -> AnyObject {
        let copy = Gratuity(value: value, locationDescription: locationDescription, email: email, data: data)
        return copy
    }
    
}