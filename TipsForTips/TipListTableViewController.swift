//
//  TipListTableViewController.swift
//  TipsForTips
//
//  Created by Claudio Kerov on 27/08/16.
//  Copyright © 2016 Claudio Kerov. All rights reserved.
//

import UIKit
import Alamofire


class TipListTableViewController: UITableViewController {

    var gratuities =  [Gratuity]()
    var server: String?
    var menuvc: MenuViewController?
    
    
    func convertCountryCodeToCountryName(code: String) -> String {
        switch code {
        case "us":
            return (self.menuvc?.usa)!
        case "it":
            return (self.menuvc?.italy)!
        case "gb":
            return (self.menuvc?.greatBritain)!
        case "jp":
            return (self.menuvc?.japan)!
        case "au":
            return (self.menuvc?.australia)!
        case "br":
            return (self.menuvc?.brazil)!
        case "me":
            return (self.menuvc?.mexico)!
        case "ru":
            return (self.menuvc?.russia)!
        case "in":
            return (self.menuvc?.india)!
        case "kr":
            return (self.menuvc?.korea)!
        case "sg":
            return (self.menuvc?.singapore)!
        case "za":
            return (self.menuvc?.southAfrica)!
        default:
            return code
        }
    }
    
    
    func getUserTips() {
        let parameters = ["email": String((self.menuvc?.currentUser!.userEmail)!)]
//        print (parameters.debugDescription)
        Alamofire.request(.GET, self.server!+"/getUserGratuities", parameters: parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success(let JSON):
//                    print (JSON)
                    let res = JSON as! NSDictionary
                    let val = res.objectForKey("response")
                    print(val.debugDescription)
                    if String(val!) != "ERROR" {
                        let list = val as? NSArray
                        if list != nil {
                            for el in list! {
                                let tmpEl = el as! NSDictionary
                                let data = tmpEl.objectForKey("data")
                                let tip = tmpEl.objectForKey("value")
                                let location = tmpEl.objectForKey("location")
                                self.gratuities.append(Gratuity(value: Int(tip! as! NSNumber), locationDescription: String(location!), email: (self.menuvc?.currentUser!.userEmail)!, data: String(data!)))
                            }
                            self.tableView.reloadData()
                        }
                    } else {
                        let alert = self.menuvc?.createAlert("", msg: (self.menuvc?.noTip)!, action: (self.menuvc?.okay)!,
                            callback: { (alert: UIAlertAction!) in
                                self.dismissViewControllerAnimated(true, completion: nil)
                        })
                        self.presentViewController(alert!, animated: true, completion: nil)
                    }
                case .Failure(let error):
                    print(error)
                    self.menuvc?.presentAlertForError(error.code, this: self)
                    return
                }
        }
    
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        getUserTips()
        self.menuvc?.setBackGroundColorForView(self.view)
        self.navigationItem.title = (self.menuvc?.listTitle)!
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if self.gratuities.count > 0 {
            rows = self.gratuities.count
        }
        return rows
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellId", forIndexPath: indexPath)

        let tip = self.gratuities[indexPath.row]
        cell.textLabel?.text = "\(String(tip.value))% \((self.menuvc?.sentFrom)!) \(self.convertCountryCodeToCountryName(tip.locationDescription)) \((self.menuvc?.at)!) \(String(tip.data))"
        cell.textLabel?.font = UIFont(name:"Helvetica Neue", size:13)
        cell.backgroundColor = UIColor(red: 230/255, green: 255/255, blue: 230/255, alpha: 1)
        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
