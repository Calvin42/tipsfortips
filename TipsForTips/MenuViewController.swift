//
//  MenuViewController.swift
//  TipsForTips
//
//  Created by claudio on 16/08/16.
//  Copyright © 2016 Claudio Kerov. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire

class MenuViewController: UITabBarController, CLLocationManagerDelegate {
    
//    var placesClient: GMSPlacesClient?
    var currentUser: User?
    var gratuity = [Gratuity]()
    var currentLocation: GeographicArea?
    var isUserLoggedIn = false
    var percentageChosen: Int?
    var server = "https://pupil42.noip.me:8087"
    var tipvc: TipsViewController?
    var navvc: UINavigationController?
    
    /*****************************
     *    STRING LOCALIZATION    *
     *****************************/
    
    //===== ALREADY ON VIEW TEXT =====//

    var description_label_default: String?
    var do_math_button: String?
    var send_tip_button: String?
    var bill_label: String?
    var hello: String?
    var userTabBar: String?
    var tipTabBar: String?
    var listLabel: String?
    var signup: String?
    var signin: String?
    var logout: String?
    var backToLogin: String?
    var toRegistration: String?
    
    //=========== ALERTS ============//

    var connection_problem_title: String?
    var connection_problem_msg: String?
    var connection_problem_action: String?
    var update_location_error: String?
    var errorOkay: String?
    var errorGotIt: String?
    var errorOops: String?
    var error: String?
    // TipsViewController
    var handleMyself: String?
    var descriptionErrorMsg: String?
    var loginFirst: String?
    var justSent: String?
    var its: String?
    var percentageQuestion: String?
    var nope: String?
    var yep: String?
    // UserViewController
    var anon: String?
    var userPage: String?
    var sureToLogout: String?
    var yesIam: String?
    var no: String?
    // LoginViewController
    var wrongCredentials: String?
    var tryAgain: String?
    var missingEmail: String?
    var missingPassword: String?
    var haveToInsert: String?
    var anEmail: String?
    var essentialPassword: String?
    // RegisterViewController
    var registrationComplete: String?
    var yay: String?
    var needAn: String?
    var needA: String?
    var email: String?
    var miswritten: String?
    var invalidEmail: String?
    var password: String?
    var username: String?
    var alreadyUsed: String?
    // TipListTableViewController
    var noTip: String?
    var okay: String?
    var sentFrom: String?
    var at: String?
    var listTitle: String?
    var italy: String?
    var usa: String?
    var greatBritain: String?
    var japan: String?
    var china: String?
    var australia: String?
    var brazil: String?
    var mexico: String?
    var russia: String?
    var india: String?
    var korea: String?
    var singapore: String?
    var southAfrica: String?
    
    
    
    func updateStringsLanguage() {
        let currentLanguage = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode) as! String
        
        self.description_label_default = "DESCRIPTION_LABEL_DEFAULT".localized(currentLanguage)
        self.do_math_button = "DO_MATH_BUTTON".localized(currentLanguage)
        self.send_tip_button = "SEND_TIP_BUTTON".localized(currentLanguage)
        self.bill_label = "BILL_LABEL".localized(currentLanguage)
        self.hello = "HELLO_USER_LABEL".localized(currentLanguage)
        self.userTabBar = "USER_TABBAR_TEXT".localized(currentLanguage)
        self.tipTabBar = "TIP_TABBAR_TEXT".localized(currentLanguage)
        self.listLabel = "LIST_LABEL".localized(currentLanguage)
        self.signin = "SIGNIN".localized(currentLanguage)
        self.signup = "SIGNUP".localized(currentLanguage)
        self.logout = "LOGOUT".localized(currentLanguage)
        self.backToLogin = "BACK_TO_LOGIN_BUTTON".localized(currentLanguage)
        self.toRegistration = "TO_REGISTRATION_BUTTON".localized(currentLanguage)
        
        self.connection_problem_title = "CONNECTION_PROBLEM_TITLE".localized(currentLanguage)
        self.connection_problem_msg = "CONNECTION_PROBLEM_MSG".localized(currentLanguage)
        self.connection_problem_action = "CONNECTION_PROBLEM_ACTION".localized(currentLanguage)
        self.update_location_error = "UPDATE_LOCATION_ERROR".localized(currentLanguage)
        self.errorOkay = "ERROR_OKAY".localized(currentLanguage)
        self.errorGotIt = "ERROR_GOT_IT".localized(currentLanguage)
        self.errorOops = "ERROR_OOPS".localized(currentLanguage)
        self.error = "ERROR".localized(currentLanguage)
        
        self.handleMyself = "HANDLE_MYSELF".localized(currentLanguage)
        self.descriptionErrorMsg = "DESCRIPTION_ERROR_MSG".localized(currentLanguage)
        self.loginFirst = "LOGIN_FIRST".localized(currentLanguage)
        self.justSent = "JUST_SENT".localized(currentLanguage)
        self.its = "IS".localized(currentLanguage)
        self.percentageQuestion = "PERCENTAGE_QUESTION".localized(currentLanguage)
        self.nope = "NOPE".localized(currentLanguage)
        self.yep = "YEP".localized(currentLanguage)
        
        self.anon = "ANON".localized(currentLanguage)
        self.userPage = "USER_PAGE".localized(currentLanguage)
        self.sureToLogout = "SURE_TO_LOGOUT".localized(currentLanguage)
        self.yesIam = "YES_IAM".localized(currentLanguage)
        self.no = "NO".localized(currentLanguage)
        
        self.wrongCredentials = "WRONG_CREDENTIALS".localized(currentLanguage)
        self.tryAgain = "TRY_AGAIN".localized(currentLanguage)
        self.missingEmail = "MISS_EMAIL".localized(currentLanguage)
        self.missingPassword = "MISS_PASSWORD".localized(currentLanguage)
        self.haveToInsert = "HAVE_TO_INSERT".localized(currentLanguage)
        self.anEmail = "AN_EMAIL".localized(currentLanguage)
        self.essentialPassword = "ESSENTIAL_PASSWORD".localized(currentLanguage)
        
        self.registrationComplete = "REGISTRATION_COMPLETE".localized(currentLanguage)
        self.yay = "YAY".localized(currentLanguage)
        self.needAn = "NEED_AN".localized(currentLanguage)
        self.needA = "NEED_A".localized(currentLanguage)
        self.invalidEmail = "INVALID_EMAIL".localized(currentLanguage)
        self.password = "PASSWORD".localized(currentLanguage)
        self.username = "USERNAME".localized(currentLanguage)
        self.alreadyUsed = "ALREADY_USED".localized(currentLanguage)
        
        self.noTip = "NO_TIP".localized(currentLanguage)
        self.okay = "OKAY".localized(currentLanguage)
        self.sentFrom = "SENT_FROM".localized(currentLanguage)
        self.at = "AT".localized(currentLanguage)
        self.listTitle = "LIST_TITLE".localized(currentLanguage)
        self.italy = "ITALY".localized(currentLanguage)
        self.usa = "USA".localized(currentLanguage)
        self.greatBritain = "GREAT_BRITAIN".localized(currentLanguage)
        self.japan = "JAPAN".localized(currentLanguage)
        self.china = "CHINA".localized(currentLanguage)
        self.australia = "AUSTRALIA".localized(currentLanguage)
        self.brazil = "BRAZIL".localized(currentLanguage)
        self.mexico = "MEXICO".localized(currentLanguage)
        self.russia = "RUSSIA".localized(currentLanguage)
        self.india = "INDIA".localized(currentLanguage)
        self.korea = "KOREA".localized(currentLanguage)
        self.singapore = "SINGAPORE".localized(currentLanguage)
        self.southAfrica = "SOUTH_AFRICA".localized(currentLanguage)
    }
    
    
    /************************
     *    USEFUL METHODS    *
     ************************/

    func createAlert(title: String, msg: String, action: String, callback: ((UIAlertAction) -> Void)! ) -> UIAlertController {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: action, style: UIAlertActionStyle.Default, handler: callback))
        
        return alert
    }
    
    
    func presentAlertForError(code: Int, this: UIViewController) {
        // CANT CONNECT TO SERVER
        if code == -1004 {
            let alert = self.createAlert(self.connection_problem_title!, msg: self.connection_problem_msg!, action: self.connection_problem_action!, callback: nil)
            this.presentViewController(alert, animated: true, completion: nil)
        }
        
        if code == -6003 {
            let alert = self.createAlert("", msg: self.alreadyUsed!, action: self.errorGotIt!, callback: nil)
            this.presentViewController(alert, animated: true, completion: nil)
        }
    }

    
    func setBackGroundColorForView(curView: UIView) {
        curView.backgroundColor = UIColor(red: 230/255, green: 255/255, blue: 230/255, alpha: 1)
    }
    

    
    /***********************/
    /*  CONTROLLER METHODS */
    /***********************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tipvc = self.tabBarController?.viewControllers![0] as? TipsViewController
        
        self.tabBar.backgroundColor = UIColor(red: 179/255, green: 179/255, blue: 179/255, alpha: 1.0)
        
        updateStringsLanguage()
        let firstTab = self.tabBar.items![0]
        let secondTab = self.tabBar.items![1]
        
        firstTab.title = self.tipTabBar!
        secondTab.title = self.userTabBar!
        firstTab.image = UIImage(named: "dollar")?.imageWithRenderingMode(UIImageRenderingMode.Automatic)
        secondTab.image = UIImage(named:"contact")?.imageWithRenderingMode(UIImageRenderingMode.Automatic)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
    }

    
  
 

}
