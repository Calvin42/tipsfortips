//
//  StringExtension.swift
//  TipsForTips
//
//  Created by Claudio Kerov on 30/08/16.
//  Copyright © 2016 Claudio Kerov. All rights reserved.
//

import Foundation


extension String {
    
    func localized(lang: String) -> String {
        let path = NSBundle.mainBundle().pathForResource(lang, ofType: "lproj")
        let bundle = NSBundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    struct NumberFormatter {
        static let instance = NSNumberFormatter()
    }
    var integerValue:Int? {
        return NumberFormatter.instance.numberFromString(self)?.integerValue
    }
}
