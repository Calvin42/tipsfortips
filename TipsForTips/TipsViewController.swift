//
//  FirstViewController.swift
//  TipsForTips
//
//  Created by Claudio Kerov on 05/05/16.
//  Copyright © 2016 Claudio Kerov. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
/*
 Controller per la main view in cui l'utente puo' scegliere la percentageuale di mancia
 ed eventualmente calcolare la cifra da lasciare
 */
import UIKit
import CoreLocation
import MapKit
import Alamofire



class TipsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate,  UITextFieldDelegate, CLLocationManagerDelegate {
    /**************/
    /* UI OUTLETS */
    /**************/
    @IBOutlet weak var percentagePicker: UIPickerView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var billTextField: UITextField!
    @IBOutlet var totalToPayLabel: UILabel!
    @IBOutlet weak var shareTipButton: UIButton!
    @IBOutlet weak var billLabel: UILabel!
    @IBOutlet weak var mathsButton: UIButton!
    
    var pickerData  = [Int]()
    var gratuitiesHash = [Int: Int]()
    var topGratuities = [Int]()

    var gratuities = [Gratuity]()
    var server = "https://pupil42.noip.me:8087"
    private var didLocationRequest = false
    private var descriptionQuerySent = false
    var didSentpercentage = false
    var menuvc: MenuViewController?
    private let defaultPercentages = [10,11,12,13,14,15,16,17,18,19,20]
    
    /*****************************
     *    STRING LOCALIZATION    *
     *****************************/
    
    func updateViewElementsLanguage() {
        self.descriptionLabel.text = (self.menuvc?.description_label_default)!
        self.shareTipButton.setTitle((self.menuvc?.send_tip_button)!, forState: UIControlState.Normal)
        self.billLabel.text = (self.menuvc?.bill_label)!
        self.mathsButton.setTitle((self.menuvc?.do_math_button)!, forState: UIControlState.Normal)
        
    }
    
    
    /*****************************/
    /*      Useful Methods       */
    /*****************************/
    

    
    // it may have multiples parameters and check for more characters, but not now
    func clean(what: String) -> String {
        var tmp = what.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        tmp = tmp.stringByReplacingOccurrencesOfString("(", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        tmp = tmp.stringByReplacingOccurrencesOfString(")", withString: "", options:  NSStringCompareOptions.LiteralSearch, range: nil)
        return tmp.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    
    func orderDictByValue(dict: [Int: Int]) {
        var tmp = Array(dict.keys)
        _ = tmp.sortInPlace() {
            let obj1 = dict[$0]
            let obj2 = dict[$1]
            return obj1 > obj2
        }
    }
    

    
    /***********************/
    /*   LOCATION METHODS  */
    /***********************/
    var locationManager = CLLocationManager()
    
    // ERROR HEANDLING
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
//        print("Error while updating location " + error.localizedDescription)
        let alert = self.menuvc?.createAlert("", msg: (self.menuvc?.update_location_error)! , action: (menuvc?.errorOkay)!, callback: nil)
        presentViewController(alert!, animated: true, completion: nil)
    }
    
    // SUCCESS HEANDLING
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            if self.didLocationRequest {
                return
            }
            if placemarks!.count > 0 {
                let pm = placemarks![0] as CLPlacemark
                self.manageLocationInfo(pm)
            } else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    
    
    func manageLocationInfo(placemark: CLPlacemark) {
        locationManager.stopUpdatingLocation()
        didLocationRequest = true
        print("\(placemark.locality!)")
        print("\(placemark.administrativeArea!)")
        print("\(placemark.country!)")
        print("\(placemark.name!)")

        let longitude = String(placemark.location!.coordinate.longitude)
        let latitude = String(placemark.location!.coordinate.latitude)
        self.menuvc?.currentLocation = GeographicArea(city: "", administrativeArea: "", country: "", description: "", latitude: latitude, longitude: longitude)
        
        getDescriptions((self.menuvc?.currentLocation!.getParameters())!)
        if self.menuvc?.gratuity.count > 0 {
            self.pickerData.removeAll()
            self.percentagePicker.reloadAllComponents()
            self.pickerData = self.defaultPercentages
            self.percentagePicker.reloadAllComponents()
        }
        getGratuities((self.menuvc?.currentLocation!.getParameters())!)
        
    }
    
    
    /**********************/
    /*    AJAX METHODS    */
    /**********************/
    
    func getDescriptions(parameters: [String: String]) {
        Alamofire.request(.GET, self.server+"/getDescription", parameters: parameters)
            .validate()
            .responseJSON(options: NSJSONReadingOptions.MutableContainers) { response in
                switch response.result {
                case .Success(let JSON):
                    print(JSON)
                    let res = JSON as! NSDictionary
                    let val = res.objectForKey("response")!
                    if String(val) != "ERROR" {
                        self.menuvc?.currentLocation!.locationDescription = self.clean(val as! String)
                    }
                    if self.menuvc?.currentLocation!.locationDescription.isEmpty == true  && self.descriptionQuerySent == false {
                        let alert = self.menuvc?.createAlert((self.menuvc?.errorOops)!, msg: (self.menuvc?.descriptionErrorMsg)!, action: (self.menuvc?.handleMyself)!, callback: nil)
                        self.presentViewController(alert!, animated: true, completion: nil)
                    } else if self.menuvc?.currentLocation!.locationDescription.isEmpty == false {
                        self.descriptionLabel.text = self.menuvc?.currentLocation!.locationDescription
                    }
                    self.descriptionQuerySent = true
                    return
                case .Failure(let error):
                        print (error)
                        self.menuvc?.presentAlertForError(error.code, this: self)
                    return
                }
        }
    }
    
    func getGratuities(parameters: [String: String]) {
        Alamofire.request(.GET, self.server+"/getPlaceGratuities", parameters: parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success(let JSON):
                    print(JSON)
                    let res = JSON as! NSDictionary
                    let val = res.objectForKey("response")!
                    if String(val) != "ERROR" {
                        let list = val as? NSArray
                        
                        // prendo le mance dalla lista fornita dal server
                        self.menuvc?.gratuity.removeAll()
                        for el in list! {
                            let tmpEl = el as! NSArray
                            self.menuvc?.gratuity.append(Gratuity(value: Int(tmpEl[0] as! NSNumber), locationDescription: "", email: "", data: ""))
                        }
                        
                        // trovo la top 3 delle mance del posto
                        self.gratuitiesHash.removeAll()
                        for tip in (self.menuvc?.gratuity)! {
                            if (self.gratuitiesHash[tip.value] != nil) {
                                self.gratuitiesHash[tip.value] = self.gratuitiesHash[tip.value]! + 1
                            } else {
                                self.gratuitiesHash[tip.value] = 1
                            }
                        }
                        if self.gratuitiesHash.count > 0 {
                            self.orderDictByValue(self.gratuitiesHash)
                            var tmp = self.gratuitiesHash
                            for _ in Range(1...3){
                                if tmp.count > 0 {
                                    self.topGratuities.append(tmp.popFirst()!.0)
                                }
                            }
                        }
                        
                        for tip in (self.menuvc?.gratuity)! {
                            // se non c'e' lo aggiungo
                            if self.pickerData.contains(tip.value) == false {
                                self.pickerData.append(tip.value)
                            }
                        }
                        print(self.gratuitiesHash.debugDescription)
                        self.percentagePicker.reloadAllComponents()
                    }
                    return
                case .Failure(let error):
                    print (error)
                    self.menuvc?.presentAlertForError(error.code, this: self)
                    return
                }
        }
        
    }
    
    
    func sendTipPercentage(parameters: [String: String]) {
        Alamofire.request(.POST, self.server+"/sendTipPercentage", parameters: parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success(let JSON):
                    let res = JSON as! NSDictionary
                    let val = res.objectForKey("response")
                    if String(val) == "OK" {
                        self.didSentpercentage = true
//                        print("OKAY")
                    }
                    return
                case .Failure(let error):
                    print (error)
                    self.menuvc?.presentAlertForError(error.code, this: self)
                    return
                }
                
        }
    }
    
    /*****************************/
    /*    Controller Methods     */
    /*****************************/
    
    func setTitleFromoutside(str: String)  {
        self.title = str
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //==========  CONTROLLER ==========//
        
        let tbvc = self.tabBarController  as! MenuViewController
        menuvc = tbvc
        self.menuvc?.setBackGroundColorForView(self.view)
        self.updateViewElementsLanguage()
        
        //======== PICKERVIEW INIT ========//
        
        percentagePicker.dataSource = self
        percentagePicker.delegate = self
        pickerData = self.defaultPercentages
        self.menuvc?.percentageChosen = pickerData[0]
        billTextField.delegate = self
//        shareTipButton.setTitle("Tip!", forState: UIControlState.Normal)
        if let amount = billTextField {
            if amount.text!.isEmpty{
                updateTotal("0")
            } else {
                updateTotal(amount.text!)
            }
        }
        
        //======== LOCATIONMANAGER ========//
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        self.locationManager.distanceFilter = 1
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.pausesLocationUpdatesAutomatically = true
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.startUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TipsViewController.willEnterForeground), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
        
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationWillEnterForegroundNotification, object: nil)
        
    }
    
    func willEnterForeground() {
        self.locationManager.startUpdatingLocation()
        self.didLocationRequest = false
    }
    
    /***************
     * PICKER VIEW *
     ***************/
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(pickerData[row])
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.menuvc?.percentageChosen = pickerData[row]
    }
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        /*
         controllo ogni riga se appartiene o meno alla lista di percentuali ottenuta
         se appartiene, controllo se fa parte dei primi tre
         se fa parte, colore verde
         altrimenti giallo
         */
        let titleData = pickerData[row]
        var color = UIColor(red: 226/255, green: 69/255, blue: 76/255, alpha: 0.9)
        for tip in (self.menuvc?.gratuity)! {
            if tip.value == titleData {
                color = UIColor(red: 255/255, green: 209/255, blue: 26/255, alpha: 0.9)
                if self.topGratuities.count > 0 && self.topGratuities.contains(tip.value) {
                    color = UIColor(red: 85/255, green: 187/255, blue: 88/255, alpha: 0.9)
                }
                break
            }
        }
        let myTitle = NSMutableAttributedString(string: String(titleData), attributes: [NSFontAttributeName:UIFont(name: "Helvetica Neue", size: 15.0)!,NSForegroundColorAttributeName:color])
        myTitle.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(18) , range: NSRange(location: 0, length: myTitle.length))
        return myTitle
    }
    

    
    /************
     * BUTTONS  *
     ************/
    
    func updateTotal(amount: String) {
        if amount == "0" {
            totalToPayLabel.text = "0"
            return
        }
        let intAmount = Double(amount)
//        print(percentageChosen)
        let doublePercentageChosen = Double((self.menuvc?.percentageChosen)!)
        let total = intAmount!*doublePercentageChosen/100 as Double
        totalToPayLabel.text = String(total)
    }
    @IBAction func calculateTotal(sender: UIButton) {
        if !self.billTextField.text!.isEmpty {
            updateTotal(self.billTextField.text!)
        }
    }
    
    @IBAction func sendTip(sender: UIButton) {
        // controlla se non ha gia' inviato una volta
        //      se si digli che non puo'
        //      se no digli di controllare che sia la percentageuale giusta e proponi cancel oppure okay
        // prendo la percentageuale selezionata nel picker e la mando al MenuVC
        // dentro il MenuVC ho un metodo che manda al db l'email utente, le coordinate
        // e la percentageuale utilizata
        
    
        if menuvc!.isUserLoggedIn == false {
            let alert = self.menuvc?.createAlert("", msg: (self.menuvc?.loginFirst)!, action: (self.menuvc?.errorGotIt)!, callback: nil)
            presentViewController(alert!, animated: true, completion: nil)
            return
        }
        if self.didSentpercentage == true {
            let alert = self.menuvc?.createAlert("", msg: (self.menuvc?.justSent)!, action: (self.menuvc?.errorOkay)!, callback: nil)
            presentViewController(alert!, animated: true, completion: nil)
            return
        } else {
            let alert = self.menuvc?.createAlert("", msg: "\((self.menuvc?.its)!) \((self.menuvc?.percentageChosen)!) \((self.menuvc?.percentageQuestion)!)", action: (self.menuvc?.yep)!,
                                                 callback: {(alert: UIAlertAction!) in
                                                    var parameters = [String:String]()
                                                    parameters["email"] = self.menuvc?.currentUser!.userEmail
                                                    parameters["value"] = String((self.menuvc?.percentageChosen)!)
                                                    parameters["latitude"] = self.menuvc?.currentLocation!.latitude
                                                    parameters["longitude"] = self.menuvc?.currentLocation!.longitude
                                                    self.sendTipPercentage(parameters)
            })
            alert!.addAction(UIAlertAction(title: (self.menuvc?.nope)!, style: UIAlertActionStyle.Default, handler: nil))
            presentViewController(alert!, animated: true, completion: nil)

        }
    }
    /*****************
     *   TEXTFIELD   *
     *****************/
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        textField.addTarget(self, action: #selector(TipsViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        // Return true so the text field will be changed
        return true
    }
    
    func textFieldDidChange(textField: UITextField){
        //        let text = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        if let _ = textField.text!.integerValue {
            print("Okay")
        } else {
            textField.text = textField.text!.stringByReplacingOccurrencesOfString("[A-Za-z]", withString: "", options: NSStringCompareOptions.RegularExpressionSearch, range: nil)
        }
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == billTextField {
            textField.resignFirstResponder()
            self.updateTotal(textField.text!)
        }
        return false
    }
    
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
}

